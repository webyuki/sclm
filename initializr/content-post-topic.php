<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>


                        <li>
                            <a href="<?php the_permalink();?>">
                                <div class="row">
                                    <div class="col-sm-3">
                                    
                                        <?php if (has_post_thumbnail()):?>
                                            <?php 
                                                // アイキャッチ画像のIDを取得
                                                $thumbnail_id = get_post_thumbnail_id();
                                                // mediumサイズの画像内容を取得（引数にmediumをセット）
                                                $eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
                                                $eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
                                            ?>
                                                <div class="topNewsTopicImg bgImg" style="background-image:url('<?php echo $eye_img_s[0];?>')"></div>
                                            <?php else: ?>
                                                <div class="topNewsTopicImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/sample01.png')"></div>
                                            <?php endif; ?>
                                    
                                    </div>
                                    <div class="col-sm-9">
                                        <p class="bold mb0"><?php the_title();?></p>
                                        <p class="mainColor text_m fontEn titleLeftBd bdPinkLeft"><?php the_time('y/m/d'); ?></p>
                                    </div>
                                </div>
                            </a>
                        </li>
