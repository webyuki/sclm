<?php
//コンテンツサイズを定義します。管理画面の[設定]-[メディア]では、画像のサイズを大中小でそれぞれ設定できます。テーマに$content_widthを定義すると、これらの値を上書きし、画像がはみ出ないようにしてくれます。
/*if ( ! isset( $content_width ) )
    $content_width = 600;
 
// ナビゲーションメニューを追加する宣言
// これにより、add_theme_support( 'menus' ) は記述する必要がありません。
register_nav_menus( array( 'primary' => __( 'Primary Navigation' ), ) );
 
// sidebar（のウィジット）を追加する宣言
register_sidebar(array(
'before_widget' => '<div id="%1$s" class="widget %2$s">',
'after_widget' => '</div>',
'before_title' => '<h3>',
'after_title' => '</h3>',
));
 
// カスタムヘッダーを追加する宣言
//  幅、高さ、デフォルトの画像、アップロード可能を定義しています
$args = array(
    'flex-width'    => true,
    'width'         => 910,
    'flex-height'    => true,
    'height'        => 80,
    'default-image' => get_template_directory_uri() . '/images/header.png',
    'uploads'       => true,
);
add_theme_support( 'custom-header', $args );
 
// カスタム背景を追加する宣言
//wp_head関数によってHTML内にCSSがbody.custom-background{ }として生成されることで背景が変更されます。
//header.phpなどに<body class="custom-background">などとするか、body_class()を使って対応させる必要があります。
$args = array(
    'default-color' => '000000',
    'default-image' => get_template_directory_uri() . '/images/background.png',
);
add_theme_support( 'custom-background', $args );
 
// フィードリンクを使用する宣言
add_theme_support('automatic-feed-links');
*/