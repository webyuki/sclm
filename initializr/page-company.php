<?php get_header(); ?>
<main>


<section class="pageHeader bgMainColor relative padding">
	<div class="bgTextBox absolute">
		<p class="bgText fontEn white">About Us</p>
	</div>
	<div class="container">
		<div class="text-center white">
			<p class="fontEn h0 fontEnNegaMb">About Us</p>
			<h3 class="h3 bold mb50 titleBd titleBdWhite">会社案内</h3>
		</div>
	</div>
</section>

<section class="topRecruit">
	<div class="flex flexPc">
		<div class="bgImg padding flexSplit pc" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_comp_01.jpg')"></div>
		<div class="topRecruitBox flexSplit">
			<div class="mainColor titleBd mb30">
				<p class="fontEn h0 fontEnNegaMb">Greeting</p>
				<h3 class="h3 bold">代表あいさつ</h3>
			</div>
			<p class="h2 mainColor bold mb30">すべてのご縁を大切に</p>
			<img class="mb10 sp" src="<?php echo get_template_directory_uri();?>/img/page_comp_01.jpg" alt="">
			
			<div class="mb30">
			
<p>当社ホームページをご覧いただき、誠にありがとうございます。</p>
<p>当社は<span class="bold">岡山県倉敷市を拠点に、中四国・近畿・九州地方を中心に運送事業を展開</span>しています。
お客様、社員、地域に方々に支えられ、おかげさまで着実に実績を重ねてきております。
数ある運送会社の中で当社の特徴は、<span class="bold">人の【心】の【和】を大切にする</span>ということです。</p>
<p>ご縁あってお仕事のご依頼をいただけるお客様、ご縁あって当社に入社してくれた仲間、
その他にも出会えた全ての方々と心で通じ、<span class="bold">和を重んじ、お客様とは末永いお付き合いが
できるパートナーとして、社員とは一緒に苦楽をともにする仲間として</span>歩んでいきたいと考えています。
今後とも心和をよろしくお願いいたします。</p>

			</div>
			<p class="bold mb0">代表取締役</p>
			<p class="bold h3">青野　孝</p>
			
		</div>
		
	</div>
</section>

<section class="margin relative">
	<div class="bgTextBox bgTextBox90 absolute">
		<p class="bgText fontEn mainColor">Company</p>
	</div>
	<div class="container">
		<div class="text-center mainColor">
			<p class="fontEn h0 fontEnNegaMb">Company</p>
			<h3 class="h3 bold mb50 titleBd">会社概要</h3>
		</div>
		<div class="pageAboutCompanyUl width720 mb100" data-aos="fade-up">
			<ul>
				<li>会社名</li>
				<li>心和流通株式会社</li>
			</ul>
			<ul>
				<li>所在地</li>
				<li>〒710-1306 岡山県倉敷市真備町有井1743-1</li>
			</ul>
			<ul>
				<li>連絡先</li>
				<li>TEL：086-441-7117　FAX：086-441-7118</li>
			</ul>
			<ul>
				<li>代表取締役</li>
				<li>青野　孝</li>
			</ul>
			<ul>
				<li>設立</li>
				<li>平成23年</li>
			</ul>
			<ul>
				<li>対応可能エリア</li>
				<li>中四国、近畿、九州エリア</li>
			</ul>
			<ul>
				<li>事業内容</li>
				<li>一般貨物、中・長距離貨物、一時預かり</li>
			</ul>
			<ul>
				<li>主要取引先</li>
				<li>アイチ物流株式会社、日生運輸株式会社、株式会社水島流通システム、他</li>
			</ul>
			<ul>
				<li>取引銀行</li>
				<li>中国銀行、トマト銀行、吉備信用金庫、広島銀行</li>
			</ul>
			<ul>
				<li>保有車両</li>
				<li>
				
<table class="table">
	<tr>
		<th>積載量</th>
		<th>形状</th>
		<th>台数</th>		
	</tr>
	<tr>
		<td>4t</td>
		<td>ウイング</td>
		<td>1台</td>
	</tr>
	<tr>
		<td>大型</td>
		<td>冷凍</td>
		<td>2台</td>
	</tr>
	<tr>
		<td>大型</td>
		<td>冷蔵</td>
		<td>1台</td>
	</tr>
	<tr>
		<td>大型</td>
		<td>平ボディ</td>
		<td>3台</td>
	</tr>
	<tr>
		<td>大型</td>
		<td>ウィング</td>
		<td>9台</td>
	</tr>
</table>				
				
				
				
				</li>
			</ul>
		</div>
	</div>
</section>
<iframe class="company_map margin" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d52524.07055027712!2d133.67177127272907!3d34.63564605117249!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x355151486a24b82b%3A0x1762ce18ba8a6f58!2z44CSNzEwLTEzMDYg5bKh5bGx55yM5YCJ5pW35biC55yf5YKZ55S65pyJ5LqV77yR77yX77yU77yT4oiS77yRIOW_g-WSjOa1gemAmu-8iOagqu-8iQ!5e0!3m2!1sja!2sjp!4v1545132817612" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>



<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>