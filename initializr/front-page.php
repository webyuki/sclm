<?php get_header(); ?>
<main>


<section class="topFvSlick relative">
    <div class="absolute topFvContact">
        <a href="<?php echo home_url();?>/#calendar" class="h3 white button bgYellow bold tra mb10"><i class="fa fa-smile-o h2" aria-hidden="true"></i><span class="bold">2回まで無料体験レッスン実施中！</span></a>
    </div>
    <ul class="topFvSlickUl">
        <li>
            <div class="topFvSlickImg bgImg relative" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_01.jpg');">
                <div class="topFvBoxText absolute bgMainColor">
                    <h2 class="h1 white bold topFvBoxTextP">AI時代に生き抜くための、<br>人間力を育てる</h2>
                </div>
            </div>
        </li>
        <li>
            <div class="topFvSlickImg bgImg relative" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_02.jpg');">
                <div class="topFvBoxText absolute bgMainColor">
                    <h2 class="h1 white bold topFvBoxTextP">正解のない時代を生きるための、<br class="pc">一つの答えがここにある。</h2>
                </div>
            </div>
        </li>
        <li>
            <div class="topFvSlickImg bgImg relative" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_05.jpg');">
                <div class="topFvBoxText absolute bgMainColor">
                    <h2 class="h1 white bold topFvBoxTextP">好きな事で活躍できる<br class="pc">基礎を育てる</h2>
                </div>
            </div>
        </li>
        <li>
            <div class="topFvSlickImg bgImg relative" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_06.jpg');">
                <div class="topFvBoxText absolute bgMainColor">
                    <h2 class="h1 white bold topFvBoxTextP">これからは“表現力”こそ、<br class="pc">生きる力になる。</h2>
                </div>
            </div>
        </li>
    </ul>
</section>


<script>

$('.topFvSlickUl').slick({
    arrows: false,
    dots: false,
    infinite: true,
    fade: true,
    speed: 1800,
    autoplay: true,
    pauseOnHover: false,
    autoplayspeed: 3000,
    cssEase: 'linear'
});
</script>


<section class="topFvNews relative mb30" data-aos="fade-up">
    <div class="container">
        <div class="bdBox topFvNewsBox bgWhite">
            <div class="row">
                <div class="col-sm-3 col-lg-2">
                    <div class="topFvNewsBoxLeft text-right pc">
                        <h3 class="bold h4">新着情報</h3>
                        <p class="fontEn mainColor text_m">Information</p>
                    </div>
                </div>
                <div class="col-sm-9 col-lg-10">
                    <ul class="slickNewsUl">
                    
                        <?php
                            //$paged = (get_query_var('page')) ? get_query_var('page') : 1;
                            $paged = get_query_var('page');
                            $args = array(
                                'post_type' =>  'post', // 投稿タイプを指定
                                'paged' => $paged,
                                'posts_per_page' => 6, // 表示するページ数
                                'orderby'=>'date',
                                'order'=>'DESC'
                                        );
                            $wp_query = new WP_Query( $args ); // クエリの指定 	
                            while ( $wp_query->have_posts() ) : $wp_query->the_post();
                                //ここに表示するタイトルやコンテンツなどを指定 
                            get_template_part('content-post-fv'); 
                            endwhile;
                            //wp_reset_postdata(); //忘れずにリセットする必要がある
                            wp_reset_query();
                        ?>		
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>



<script>
$('.slickNewsUl').slick({
    autoplay:true,
    dots:false,
    arrows:true,
    autoplaySpeed: 5000,
    speed:800,
    cssEase:'ease'
    
});
</script>





<section class="margin padding bgTroubleColor">
    <div class="container">
        <div class="text-center mb50">
            <h3 class="h3 bold">お子様と向き合う中で、こんな想いはありませんか？</h3>
        </div>
        <div class="width980 bgWhite mb50" data-aos="fade-up">
            <ul class="pageReformTroubleUl">
                <li><i class="fa fa-check-circle h2 mainColor" aria-hidden="true"></i><span class="bold h4">好きなことや得意なことで、将来仕事を生み出せる人になって欲しい。</span></li>
                <li><i class="fa fa-check-circle h2 mainColor" aria-hidden="true"></i><span class="bold h4">我が子の個性が何か、色々経験させてみたい。</span></li>
                <li><i class="fa fa-check-circle h2 mainColor" aria-hidden="true"></i><span class="bold h4">激動する時代を生き抜く「人間力」や「表現力」をつけさせたい。</span></li>
                <li><i class="fa fa-check-circle h2 mainColor" aria-hidden="true"></i><span class="bold h4">自分の想いを自分の言葉で人に伝えられる子に育ってほしい。</span></li>
                <li><i class="fa fa-check-circle h2 mainColor" aria-hidden="true"></i><span class="bold h4">この子の魅力を引き出し、自己肯定力を高めたい。</span></li>
                <li><i class="fa fa-check-circle h2 mainColor" aria-hidden="true"></i><span class="bold h4">自分の力、能力で社会に貢献できる人に育てたい。</span></li>
            </ul>
        </div>
        <div class="text-center">
            <h5 class="bold h3 mainColor titleBd mb20">1つでも当てはまる方は、ぜひ一度、<br>スクラムキッズアカデミーにお越しください</h5>
        </div>
    </div>
</section>


<section class="margin" id="concept">
    <div class="container">
        <div class="text-center">
            <div class="mb50" data-aos="fade-up">
                <h3 class="bold h1"><ruby><span class="mainColor bold">ス</span><span class="colorBlue bold">ク</span><span class="colorYellow bold">ラ</span><span class="colorGreen bold">ム</span><rt><span class="mainColor bold">S</span><span class="colorBlue bold">C</span><span class="colorYellow bold">L</span><span class="colorGreen bold">M</span></rt></ruby>キッズアカデミーとは？</h3>
                <p class="fontEn mainColor h4">Concept</p>
            </div>
            
            <p class="bold h3 mb30">好きな事で活躍できる基礎を育てる</p>
            <!--<p class="bold h3 mb30">４つのバランス教育で、変化の時代に強くなる。</p>-->
            <!--<p class="bold h3 mb30">４つのバランス教育で、変化の時代に強くなる。</p>-->
            <p class="lh_xl bold mb30">人の多様性とAIが共存するこれからの時代。<br>自分自身がブランドになり好きな事で活躍できる、基礎を育てる。<br>それが“SCLM教育”です。<br>「スクラムキッズアカデミー」は10 歳までのゴールデンエイジに<br>お子様固有の「魅力と才能」を引き出す 4 つの作用となる<br>「<span class="fontEn mainColor">Sports</span>×<span class="colorBlue fontEn">Creativity</span>×<span class="fontEn colorYellow">Language</span>×<span class="fontEn colorGreen">Music</span>」を連携させた育成システムです。<br>AI 時代を生き抜くために必要な<br>「キラリと光る人間力」を育んでいきます。</p>
        </div>
        <div class="row" data-aos="fade-up">
            <div class="col-sm-3">
                <div class="topConceptBox sports mb30">
                    <div class="topConceptImgBox bgImgCircle bgImg relative mb20" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_concept_s_img.jpg')">
                        <img src="<?php echo get_template_directory_uri();?>/img/top_concept_s_ico.png" alt="運動神経を伸ばす" class="topConceptIco absolute">
                    </div>
                    <div class="text-center relative mb10">
                        <h4 class="bold h4">運動神経を<br>伸ばす</h4>
                        <p class="fontEn h0 absolute topConceptEn mainColor">Sports</p>
                    </div>
                    <p class="bold h3 mainColor text-center mb30">たいそうクラス</p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="topConceptBox creativity mb30">
                    <div class="topConceptImgBox bgImgCircle bgImg relative mb20" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_concept_c_img.jpg')">
                        <img src="<?php echo get_template_directory_uri();?>/img/top_concept_c_ico.png" alt="発想力・対話力を身につける" class="topConceptIco absolute">
                    </div>
                    <div class="text-center relative mb10">
                        <h4 class="bold h4">発想力・対話力を<br>身につける</h4>
                        <p class="fontEn h0 absolute topConceptEn colorBlue">Creativity</p>
                    </div>
                    <p class="bold h3 colorBlue text-center mb30">子どもかいぎクラス</p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="topConceptBox language mb30">
                    <div class="topConceptImgBox bgImgCircle bgImg relative mb20" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_concept_l_img_02.jpg')">
                        <img src="<?php echo get_template_directory_uri();?>/img/top_concept_l_ico.png" alt="対話力・共感力を身につける" class="topConceptIco absolute">
                    </div>
                    <div class="text-center relative mb10">
                        <h4 class="bold h4">グローバルコミュニケーション力を身につける</h4>
                        <p class="fontEn h0 absolute topConceptEn colorYellow">Language</p>
                    </div>
                    <p class="bold h3 colorYellow text-center mb30">英語クラス</p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="topConceptBox music mb30">
                    <div class="topConceptImgBox bgImgCircle bgImg relative mb20" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_concept_m_img.jpg')">
                        <img src="<?php echo get_template_directory_uri();?>/img/top_concept_m_ico.png" alt="リズム感を身につける" class="topConceptIco absolute">
                    </div>
                    <div class="text-center relative mb10">
                        <h4 class="bold h4">リズム感を<br>身につける</h4>
                        <p class="fontEn h0 absolute topConceptEn colorGreen">Music</p>
                    </div>
                    <p class="bold h3 colorGreen text-center mb30">ダンスクラス</p>
                </div>
            </div>
        </div>
        <div class="text-center">
            <a href="<?php echo home_url();?>/#course" class="h4 white button bold tra mb10">詳しいコースプランはこちら</a>
        </div>
    </div>
</section>

<section class="padding topFeature" id="reason">
    <div class="container">
        <div class="row mb50">
            <div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
                <div class="mb30">
                    <h3 class="bold h1"><ruby><span class="mainColor bold">ス</span><span class="colorBlue bold">ク</span><span class="colorYellow bold">ラ</span><span class="colorGreen bold">ム</span><rt><span class="mainColor bold">S</span><span class="colorBlue bold">C</span><span class="colorYellow bold">L</span><span class="colorGreen bold">M</span></rt></ruby>キッズアカデミーが<br>大切にしている想い</h3>
                    <p class="fontEn mainColor h4">Feature</p>
                </div>
                <!--<p class="bold h3 mb30 titleLeftBd">4つのバランス教育で養う力。</p>-->
                <p class="bold h3 mb30 titleLeftBd">人生の主役は自分。</p>
                
                <p class="lh_xl bold mb30">自分が本当に好きなこと、やりたいことは何なのか？<br>自分の強みは何なのか？<br>
人生は、この問いと、トライ&amp;エラーのくり返し。<br>人生のゴールデンエイジである幼児期から、<br>様々な豊かな体験を通じ、<br>大人から与えられることだけでなく、<br>自分で楽しいことややりたいことを見つけ、<br>自信をもって自分の人生を切り拓いていって欲しいと願い、<br>スクラムキッズアカデミーは生まれました。</p>
                <!--
                <p class="bold h3 mb30 titleLeftBd">正解のない時代を生きるための、一つの答えがここにある。</p>
<p class="lh_xl bold mb30">この子は、何が好き?<br>やりたいことってなんだろう?<br>この子の将来に本当に役立つものは?<br>
</p>-->

            </div>
            <div class="col-sm-6 col-sm-pull-6 mb50" data-aos="fade-right">
                <img src="<?php echo get_template_directory_uri();?>/img/top_feature_illust.png" alt="スクラムSCLMキッズアカデミーが大切にしている想い" class="topFeatureIllust">
            
            </div>
        </div>
    </div>
</section>



<section class="margin" id="lesson">
    <div class="container">
        <div class="text-center mb50">
            <div class="mb30">
                <h3 class="bold h1">レッスン内容</h3>
                <p class="fontEn mainColor h4">Lesson</p>
            </div>
            <p class="h3 text-center bold mainColor mb30">10歳までに身につけておきたい、一生役立つ2つの力を伸ばします</p>
            <p class="lh_xl bold">スクラムは学校では教えてくれない、人生で実際に役立つ内容が満載。<br>1つの正解ではなく、子どもたちの中にそれぞれ答えがある。<br>それを引き出し、伸ばしていきたいと考えます。</p>
        </div>
        <div class="topLessonBox relative mb50" data-aos="fade-up">
            <div class="row">
                <div class="col-sm-6 col-sm-push-6">
                    <!--<p class="fontEn h4"><span class="h1 mainColor">S</span>ports &amp; <span class="h1 colorGreen">M</span>usic</p>-->
                    <h4 class="mainColor bold h1 mb10">からだ</h4>
                    <p class="bold h4 mb30">楽しく体を動かし「できない」→「できた！」を繰り返し、「自信」と「がんばる楽しさ」を育む</p>
                    <div class="topLessonPickupBox flow bgWhite relative">
                        <p class="topLessonPickupTitle bold h4 absolute inlineBlock bgWhite flow"><i class="fa fa-child" aria-hidden="true"></i> たいそうクラスの流れ</p>
                        <ul class="">
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn mainColor h4">1. </span>音楽にのってウォーミングアップ</h5>
                                <p class="text_m gray">様々なウォーキングで、体をほぐし、ととのえます。</p>
                            </li>
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn mainColor h4">2. </span>スタートダッシュ</h5>
                                <p class="text_m gray">様々なスタートダッシュで、瞬発力、集中力、フットワークを身につけます。</p>
                            </li>
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn mainColor h4">3. </span>姿勢</h5>
                                <p class="text_m gray">正しい姿勢を知り、体の歪みを整え、正常な骨格を形成します。それにより、体の機能を最大限引き出し、運動能力も高めます。</p>
                            </li>
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn mainColor h4">4. </span>準備たいそう</h5>
                                <p class="text_m gray">リズムにのって体の機能性を高めるアイソレーション、柔軟たいそう、補強運動、体幹・バランストレーニング、コーディネーション運動などを行います。</p>
                            </li>
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn mainColor h4">5. </span>2ヶ月ごとに変わる主運動</h5>
                                <p class="text_m gray">マット運動、鉄棒、とび箱、ボール運動、輪っか、縄跳び などを行います。</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                    <div class="pageTrailerTytpeSlick">
                        <ul class="slickThumbJs mb20">
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_18.jpg" alt="カラダ × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_s_img_01.jpg" alt="カラダ × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_s_img_02.jpg" alt="カラダ × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_s_img_03.jpg" alt="カラダ × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_s_img_04.jpg" alt="カラダ × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_s_img_05.jpg" alt="カラダ × 表現力">
                                </div>
                            </li>
                        </ul>
                        <ul class="slickThumbJsNav">
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_18.jpg" alt="カラダ × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_s_img_01.jpg" alt="カラダ × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_s_img_02.jpg" alt="カラダ × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_s_img_03.jpg" alt="カラダ × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_s_img_04.jpg" alt="カラダ × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_s_img_05.jpg" alt="カラダ × 表現力">
                            </li>
                        </ul>
                    </div>
                    <div class="topLessonPickupBox bgWhite relative">
                        <p class="topLessonPickupTitle bold h4 absolute inlineBlock bgWhite"><i class="fa fa-hand-o-right" aria-hidden="true"></i>ココが伸びる！</p>
                        <ul class="topFeatureTagUl inlineBlockUl white text_m">
                            <li>運動神経を伸ばす</li>
                            <li>リズム音感</li>
                            <li>印象力</li>
                            <li>心身の健康</li>
                            <li>世界で通用する姿勢と歩き方</li>
                            <li>柔軟性</li>
                            <li>体幹</li>
                            <li>バランス力</li>
                            <li>瞬発力</li>
                            <li>体の可動域アップ</li>
                            <li>しなやかで強いカラダ</li>
                            <li>表現力</li>
                            <li>脳の活性</li>
                            <li>体育が好きになる</li>
                            <li>運動会で活躍できる</li>
                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
        
        <div class="topLessonBox relative mb50 even creativity" data-aos="fade-up">
            <div class="row">
                <div class="col-sm-6">
                    <!--<p class="fontEn h4"><span class="h1 colorBlue">C</span>reativity &amp; <span class="h1 colorYellow">L</span>anguage</p>-->
                    <h4 class="colorBlue bold h1 mb10">ことば</h4>
                    <p class="bold h4 mb30">自分の気持ちや考えを自分のことばで伝える力を育み、コミュニケーションに自信がつく！</p>
                    <div class="topLessonPickupBox flow bgWhite relative">
                        <p class="topLessonPickupTitle bold h4 absolute inlineBlock bgWhite flow"><i class="fa fa-child" aria-hidden="true"></i> 子どもかいぎクラスの流れ</p>
                        <ul class="">
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn colorBlue h4">1. </span>アイスブレーク</h5>
                                <p class="text_m gray">言葉や体をつかったゲームです。</p>
                            </li>
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn colorBlue h4">2. </span>発声練習、早口言葉であそぶ</h5>
                            </li>
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn colorBlue h4">3. </span>テーマ決め</h5>
                                <p class="text_m gray">用意したいくつかのテーマの中から子どもたちがテーマを決めます。</p>
                            </li>
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn colorBlue h4">4. </span>簡単なお約束やルール説明</h5>
                                <p class="text_m gray">姿勢、態度、表情などについても説明します。</p>
                            </li>
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn colorBlue h4">5. </span>テーマに沿って自由にディスカッション</h5>
                            </li>
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn colorBlue h4">6. </span>２人組または３人組に分かれて意見をまとめる</h5>
                            </li>
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn colorBlue h4">7. </span>代表者が前に出て発表する</h5>
                            </li>
                            <li class="mb10">
                                <h5 class="bold h5 mb0"><span class="fontEn colorBlue h4">8. </span>感想タイム</h5>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="pageTrailerTytpeSlick">
                        <ul class="slickThumbJs mb20">
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_01.jpg" alt="考える力 × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_02.jpg" alt="考える力 × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_03.jpg" alt="考える力 × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_10.jpg" alt="考える力 × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_11.jpg" alt="考える力 × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_12.jpg" alt="考える力 × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_13.jpg" alt="考える力 × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_14.jpg" alt="考える力 × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_15.jpg" alt="考える力 × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_16.jpg" alt="考える力 × 表現力">
                                </div>
                            </li>
                            <li>
                                <div class="relative">
                                    <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_17.jpg" alt="考える力 × 表現力">
                                </div>
                            </li>
                        </ul>
                        <ul class="slickThumbJsNav">
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_01.jpg" alt="考える力 × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_02.jpg" alt="考える力 × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_03.jpg" alt="考える力 × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_10.jpg" alt="考える力 × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_11.jpg" alt="考える力 × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_12.jpg" alt="考える力 × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_13.jpg" alt="考える力 × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_14.jpg" alt="考える力 × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_15.jpg" alt="考える力 × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_16.jpg" alt="考える力 × 表現力">
                            </li>
                            <li>
                                <img class="" src="<?php echo get_template_directory_uri();?>/img/top_lesson_c_img_17.jpg" alt="考える力 × 表現力">
                            </li>
                        </ul>

                    </div>
                    <div class="topLessonPickupBox bgWhite relative">
                        <p class="topLessonPickupTitle bold h4 absolute inlineBlock bgWhite"><i class="fa fa-hand-o-right" aria-hidden="true"></i>
        子どもかいぎテーマ 一例</p>
                        <p class="text_m">※子どもたちと話をしていく中で、変更になる場合もございます。</p>
                        <ul class="topFeatureTagUl inlineBlockUl white text_m">
                            <li>自分の好きなこと得意なことさがし</li>
                            <li>友だちって何？ケンカした時どうする？</li>
                            <li>夢ってなに？どうやって叶えるの？</li>
                            <li>お仕事（天職）さがし</li>
                            <li>お金って何？何のためにあるの？</li>
                            <li>自己紹介（他者紹介）をしてみよう</li>
                            <li>人をほめ、勇気づけてあげよう（褒め褒めワーク）</li>
                            <li>自信ってなに？どうやったら自信がもてるの？</li>
                        </ul>
                    </div>
                    



                    
                    
                    
                    <!--
                    <div class="topLessonPickupBox bgWhite relative">
                        <p class="topLessonPickupTitle bold h4 absolute inlineBlock bgWhite"><i class="fa fa-hand-o-right" aria-hidden="true"></i>
        ココが伸びる！</p>
                        <ul class="topFeatureTagUl inlineBlockUl white text_m">
                            <li>考える力</li>
                            <li>話す力</li>
                            <li>聞く力</li>
                            <li>表現力</li>
                            <li>信頼力</li>
                            <li>創造性</li>
                            <li>問題解決力</li>
                            <li>自己肯定力</li>
                            <li>経営者思考</li>
                            <li>集中力</li>
                            <li>多様性</li>
                            <li>共感力</li>
                            <li>自信</li>
                            <li>ポジティブ思考</li>
                            <li>コミュニケーション力</li>
                            <li>スピーチ力</li>
                            <li>プレゼンテーション力</li>
                            <li>リーダーシップ力（副リーダー）</li>
                            <li>チーム力　</li>
                        </ul>
                    </div>
                    -->
                </div>
            </div>
            
        </div>        
        
        
    </div>
</section>





<script>


 $('.slickThumbJs').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slickThumbJsNav'
});
$('.slickThumbJsNav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slickThumbJs',
  dots: false,
  centerMode: true,
  focusOnSelect: true
});
	
</script>



<section class="padding topCourse" id="course">
    <div class="container">
        <div class="text-center mb50">
            <div class="mb30">
                <h3 class="bold h1">料金・クラス</h3>
                <p class="fontEn mainColor h4">Class</p>
            </div>
        </div>
        <div class="row mb30">

            <div class="col-sm-6">
                <div class="topCoursePlanBox bgWhite bdBox" data-aos="fade-up">
                    <h5 class="bgMainColor white bold h3 text-center mb0 topCoursePlanBoxTitle">たいそうクラス</h5>
                    <p class="white bold h5 text-center mb30 topCoursePlanBoxTitleSub">からだ</p>
                    <div class="topCoursePlanDlWrap">
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">月謝</div></dt>
                            <dd>
                                <span class="fontEn h2 mainColor">3,500</span>円(税別)/月<span class="fontEn h2 mainColor">2</span>回<br>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">対象</div></dt>
                            <dd>
                                <p class="mb0">・幼児（年少〜年長）</p>
                                <p class="mb0">・Jr（小1〜3年生）</p>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">曜日</div></dt>
                            <dd>
                                <p class="mb0">・月曜日（渡辺はるみ担当）</p>
                                <p class="mb0">・木曜日（にれいあやこ担当）</p>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">時間</div></dt>
                            <dd>
                                <p class="text_m"><span class="bold mainColor h5">月曜日</span><br>・幼児：16:15〜17:00<br>・Jr：17:15〜18:00</p>
                                <p class="text_m"><span class="bold mainColor h5">木曜日</span><br>・幼児：16:00〜16:45<br>・Jr：17:15〜18:00</p>
                                <span class="text_s gray">※詳しくは<a href="#calendar" class="linkText mainColor">カレンダー<i class="fa fa-external-link" aria-hidden="true"></i></a>をご覧ください</span>
                                </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">内容</div></dt>
                            <dd><p class="text_m gray">10歳までのゴールデンエイジの今、楽しみながら様々な動きを通じてカラダの基礎をつくります。この時期にしか伸びない運動神経を伸ばし、運動能力をグングン伸ばしながらやる気！勇気！集中力！を養います。</p></dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt>
                                <div class="topCoursePlanTitle">講師紹介</div>
                                <div class="topCourseOptionImgCircleWrap">
                                    <div class="bgImgCircle bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_price_option_tea_06.jpg');"></div>
                                </div>
                            </dt>
                            <dd>
                                <p class="bold h5">渡辺はるみ</p>
                                <p class="text_m gray mb30">東広島幼児体育研究所所長。幼児体育一筋40年以上。教えた生徒は1万人以上の大ベテラン。</p>
                                <p class="bold h5">にれい あやこ</p>
                                <p class="text_m gray">スクラムキッズアカデミー代表。実家が幼児体育研究所という環境に育ち、20代の頃から幼児体育指導員として活動。(木曜担当)</p>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="topCoursePlanBox bgWhite bdBox pre even" data-aos="fade-up">
                    <h5 class="bgBlue white bold h3 text-center mb0 topCoursePlanBoxTitle">子どもかいぎクラス</h5>
                    <p class="white bold h5 text-center mb30 topCoursePlanBoxTitleSub">ことば</p>
                    <div class="topCoursePlanDlWrap">
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">月謝</div></dt>
                            <dd>
                                <span class="fontEn h2 colorBlue">3,500</span>円(税別)/月<span class="fontEn h2 colorBlue">2</span>回<br>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">対象</div></dt>
                            <dd>
                                <p class="mb0">・幼児（年少〜年長）</p>
                                <p class="mb0">・Jr（小1〜3年生）</p>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">曜日</div></dt>
                            <dd>
                                <p class="mb0">・木曜日</p>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">時間</div></dt>
                            <dd>
                                <p class="text_m">幼児：16:00-16:45<br>Jr：17:15〜18:00</p>
                                <span class="text_s gray">※詳しくは<a href="#calendar" class="linkText mainColor">カレンダー<i class="fa fa-external-link" aria-hidden="true"></i></a>をご覧ください</span>
                                </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">内容</div></dt>
                            <dd><p class="text_m gray">自分の思いを大切にする事と、その思いをことばで伝える力はこれからの時代とても重要です。子どもの「心の声」「言いたい事」を安心して話せる環境をつくり、答えが一つではない様々な課題についてみんなで話し合い発表する場です。</p></dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt>
                                <div class="topCoursePlanTitle">講師紹介</div>
                                <div class="topCourseOptionImgCircleWrap">
                                    <div class="bgImgCircle bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_price_option_tea_07.jpg');"></div>
                                </div>
                            </dt>
                            <dd>
                                <p class="bold h5">にれい あやこ</p>
                                <p class="text_m gray">子どもの好きや得意を伸ばし社会で活躍できる子を増やしたいと2016年にスタジオスクラムを設立。<br><span class="text_s gray">※詳しくは<a href="#prof" class="linkText mainColor">プロフィール<i class="fa fa-external-link" aria-hidden="true"></i></a>をご覧ください</span>
</p>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <!--
        <div class="text-center mb30">
            <h4 class="h2 titleBd bold">オプションコース</h4>
        </div>
        <div class="text-center mb50">
            <p class="lh_xl">スクラムでは基本コース・幼児コースを選択いただいた方にはオプションコースを提供しています。<br>オプションコースのみのお申込みも可能ですが、それぞれ費用が異なります。</p>
        </div>
        -->
        <div class="width720">
            <div class="bgTroubleColor bdBox topPriceSetBox text-center mb30">
                <p class="h2 bold titleBd mb30">たいそう&amp;子どもかいぎはセットでオトク！</p>
                <p class="h3 bold">おすすめ！スクラムコース</p>
                <p class="mainColor h1 bold mb20"><span class="h5 bold">月</span>4<span class="h5 bold">回 </span><span class="lineThrough h3">7,000円</span><span class="bold h4"> → </span>6,000<span class="h5 bold">円</span></p>
                <p>からだとことば両方からの相乗効果で、<br class="pc">お子様の力を伸ばしていきます</p>
            </div>
        </div>
        
        <div class="row mb30">
            <div class="col-sm-6">
                <div class="topCoursePlanBox bgWhite bdBoxYellow" data-aos="fade-up">
                    <h5 class="bgYellow white bold h3 text-center mb0 topCoursePlanBoxTitle">英語クラス</h5>
                    <p class="white bold h5 text-center mb30 topCoursePlanBoxTitleSub">ことば</p>
                    <div class="topCoursePlanDlWrap">
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">月謝</div></dt>
                            <dd>
                                <span class="fontEn h2 colorYellow">6,000</span>円(税別)/月<span class="fontEn h2 colorYellow">4</span>回<br>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">対象</div></dt>
                            <dd>
                                <p class="mb0">・入門（3〜4歳）</p>
                                <p class="mb0">・幼児（年少〜年長）</p>
                                <p class="mb0">・Jr（小1〜3年生）</p>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">時間</div></dt>
                            <dd>
                                <p class="text_m">・入門：15:30〜16:10<br>・幼児：16:15〜17:00<br>・Jr：17:15〜18:00</p>
                                <span class="text_s gray">※詳しくは<a href="#calendar" class="linkText mainColor">カレンダー<i class="fa fa-external-link" aria-hidden="true"></i></a>をご覧ください</span>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">内容</div></dt>
                            <dd><p class="text_m gray">学校教員資格もあるネイティブ外国人講師による“生きた英語力”“国際的なコミュニケーション力”を身につける実践的な英会話レッスン♪カード・歌・ゲーム絵本・フォニックス等の活動を通じて英語を楽しみ自信をもって話せるようになります。</p></dd>
                        </dl>
                      <dl class="topCoursePlanDl flex alignCenter">
                            <dt>
                                <div class="topCoursePlanTitle">講師紹介</div>
                                <div class="topCourseOptionImgCircleWrap">
                                    <div class="bgImgCircle bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_price_option_tea_05.jpg');"></div>
                                </div>
                            </dt>
                            <dd>
                                <p class="bold h5">ポール</p>
                                <p class="text_m gray">カナダの教員免許を持ち、カナダの学校で10年以上日本の小学校で10年以上の教育経験がある。“楽しく分かりやすく”をモットーに英会話レッスンを行っている。</p>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="topCoursePlanBox bgWhite bdBoxGreen even" data-aos="fade-up">
                    <h5 class="bgGreen white bold h3 text-center mb0 topCoursePlanBoxTitle">ダンスクラス</h5>
                    <p class="white bold h5 text-center mb30 topCoursePlanBoxTitleSub">からだ</p>
                    <div class="topCoursePlanDlWrap">
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">月謝</div></dt>
                            <dd>
                                <p><span class="h4 bold">幼児：</span><span class="fontEn h2 colorGreen">3,500</span>円(税別)/月<span class="fontEn h2 colorGreen">3</span>回<br><span class="h4 bold">Jr：</span><span class="fontEn h2 colorGreen">5,000</span>円(税別)/月<span class="fontEn h2 colorGreen">3</span>回</p>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">対象</div></dt>
                            <dd>
                                <p class="mb0">・幼児（年少〜年長）</p>
                                <p class="mb0">・Jr（小学1〜3年生位）</p>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">時間</div></dt>
                            <dd>
                                <p class="text_m">・幼児：16:30〜17:00<br>・Jr：17:15〜18:00</p>
                                <span class="text_s gray">※詳しくは<a href="#calendar" class="linkText mainColor">カレンダー<i class="fa fa-external-link" aria-hidden="true"></i></a>をご覧ください</span>
                                </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">内容</div></dt>
                            <dd><p class="text_m gray">音楽にのって、様々なリズムをとったりJ-POPやアイドルソングなどの耳慣れた曲で楽しくカッコよく踊ります♪踊るの大好き!な子もこれから踊ってみたい子も、基礎から踊れるカラダをつくっていきます♪</p></dd>
                        </dl>
                      <dl class="topCoursePlanDl flex alignCenter">
                            <dt>
                                <div class="topCoursePlanTitle">講師紹介</div>
                                <div class="topCourseOptionImgCircleWrap">
                                    <div class="bgImgCircle bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_price_option_tea_04.jpg');"></div>
                                </div>
                            </dt>
                            <dd>
                                <p class="bold h5">TAMI</p>
                                <p class="text_m gray">5歳より クラシックバレエを始め、芸術大学にてジャズダンス モダンダンス ヒップホップ エアロビクスetc.を学ぶ。卒業後、NY 、LA、東京などで学びを深め、様々な舞台に出演。USJのオープニングダンサーも務める。結婚後２人の子育てをしながら、現在もインストラクター&amp;ダンサーとして活躍中。</p>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>        
        
        <!--
        <div class="row mb30">
            <div class="col-sm-6">
                <div class="topCoursePlanBox bgWhite bdBoxPurple" data-aos="fade-up">
                    <h5 class="bgPurple white bold h3 text-center mb0 topCoursePlanBoxTitle">バレエクラス</h5>
                    <p class="white bold h5 text-center mb30 topCoursePlanBoxTitleSub">からだ</p>
                    <div class="topCoursePlanDlWrap">
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">月謝</div></dt>
                            <dd>
                                <span class="fontEn h2 colorPurple">6,000</span>円(税別)/月<span class="fontEn h2 colorPurple">4</span>回<br>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">対象</div></dt>
                            <dd>
                                <p class="mb0">・入門（3〜4歳）</p>
                                <p class="mb0">・幼児（年少〜年長）</p>
                                <p class="mb0">・Jr（小1〜3年生）</p>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">時間</div></dt>
                            <dd>
                                <p class="text_m">・入門：15:30〜16:10<br>・幼児：16:15〜17:00<br>・Jr：17:15〜18:00</p>
                                <span class="text_s gray">※詳しくは<a href="#calendar" class="linkText mainColor">カレンダー<i class="fa fa-external-link" aria-hidden="true"></i></a>をご覧ください</span>
                            </dd>
                        </dl>
                        <dl class="topCoursePlanDl flex alignCenter">
                            <dt><div class="topCoursePlanTitle">内容</div></dt>
                            <dd><p class="text_m gray">音楽に合わせて踊りながら、少しずつ、柔軟性や身体の強さ、表現力、集中力、バレエのポジションや動きを身につけていきます。まずは一緒に楽しく踊りましょう！</p></dd>
                        </dl>
                      <dl class="topCoursePlanDl flex alignCenter">
                            <dt>
                                <div class="topCoursePlanTitle">講師紹介</div>
                                <div class="topCourseOptionImgCircleWrap">
                                    <div class="bgImgCircle bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_price_option_tea_08.jpg');"></div>
                                </div>
                            </dt>
                            <dd>
                                <p class="bold h5">MIKA</p>
                                <p class="text_m gray">3歳よりバレエを始め、2006年ロシア国立ノヴォシビルスクバレエ学校へ短期留学。2009年～2010年Northern Ballet Schoolへ留学。帰国後子どもから大人までバレエの指導を行う。2013年スターダンサーズ・バレエ団へ入団。2020年の退団まで様々な公演へ出演。現在もダンサー、指導者として活動中。</p>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>    
        -->    
        


        <!--<div class="width980">
            <div class="bgWhite bdBox topPriceSetBox text-center mb30">
                <p class="h3 bold titleBd mb20">その他にこんなセットも</p>
                <p class="bold">度胸を身につけたいなら、<span class="h3 colorYellow bold">英語</span> <span class="bold h4">+</span> <span class="h3 colorGreen bold">ダンス</span> <span class="bold h3"><span class="lineThrough h4">11,500円</span><span class="bold h4"> → </span>10,500円</span></p>
                <p class="bold mb30">体をつかった表現力をのばすなら、<span class="h3 mainColor bold">たいそう</span> <span class="bold h4">+</span> <span class="h3 colorGreen bold">ダンス</span> <span class="bold h3"><span class="lineThrough h4">8,500円</span><span class="bold h4"> → </span>7,500円</span></p>
                <p class="h3 bold titleBd mb20">TOTALセットはこんなにおトク！</p>
                <p class="bold"><span class="h3 mainColor bold">たいそう</span><span class="bold h4"> + </span><span class="h3 colorBlue bold">子どもかいぎ</span><span class="bold h4"> + </span><span class="h3 colorYellow bold">英語</span><span class="bold h4"> + </span><span class="h3 colorGreen bold">ダンス</span> <span class="bold h3"><span class="lineThrough h4">18,500円</span><span class="bold h4"> → </span>15,000円</span></p>
            </div>
        </div>-->
        <div class="width980">
            <div class="bgWhite bdBox topPriceSetBox text-center mb30">
                <p class="h3 bold titleBd mb20">その他にこんなセットも</p>
                <p class="bold">グローバルな表現力を身につけるには、<span class="h3 colorYellow bold">英語</span> <span class="bold h4">+</span> <span class="h3 colorGreen bold">ダンス</span> <span class="bold h3"><span class="bold h4"> → </span>8,500円</span> 〜 10,000円 / 月7回</p>
                <p class="bold mb30">身体能力と思考力の両方を身につけるには、<span class="h3 mainColor bold">たいそう</span> <span class="bold h4">+</span> <span class="h3 colorBlue bold">こどもかいぎ</span> <span class="bold h3"><span class="bold h4"> → </span>6,000円</span> / 月4回</p>
                <p class="bold mb30">しなやかで柔軟な美しい体をつくるには、<span class="h3 colorPurple bold">バレエ</span> <span class="bold h4">+</span> <span class="h3 mainColor bold">たいそう</span> <span class="bold h3"><span class="bold h4"> → </span>8,500円</span> / 月6回</p>
            </div>
        </div>

       

                                     
                       
                               
                                               
        <div class="width720 text_m gray mb50">
            <p>※入会手数料5,000円(税別)が入会時に発生します。<br>
            ※年間設備2,000円(税別)、年間スポーツ保険料800円(税別)が発生します。<br>
            ※各クラス定員6〜8名。人数の都合でご希望に沿えない場合はご了承ください。</p>
        </div>
        <div class="text-center mb30" id="calendar">
            <h4 class="h2 titleBd bold">スケジュール・ご予約カレンダー</h4>
        </div>
        <div class="text-center mb30">
            <!--<p class="h3 bold mb20 mainColor">無料体験レッスンは6月から開催しています</p>-->
            <a href="line://ti/p/%40pmn7040l" target="_blank" class="h5 white button line bold tra mb20"><i class="fa fa-comment-o h4" aria-hidden="true"></i><span class="bold">LINEでも随時ご予約受付可能！</span></a>
        </div>
        <div class="text-center">
                
                    
            <div class="scheduleIframe width980 text-center">
                <iframe src="https://web.star7.jp/reserve_new/mobile_yoyaku_101.php?p=315c0e0241&nodispheadfoot=1" width="100%" height="700"></iframe>
            </div>
            
        </div>
    </div>
</section>

<?php get_template_part( 'parts/temp-contact' ); ?>				


<section class="margin" id="prof">
    <div class="container">
        <div class="text-center mb50">
            <div class="mb30">
                <h3 class="bold h1">プロフィール</h3>
                <p class="fontEn mainColor h4">Profile</p>
            </div>
        </div>
        <div class="row mb50">
            <div class="col-sm-6 col-sm-push-6" data-aos="fade-left">
                <div class="topProfImgWrap mb50">
                    <img src="<?php echo get_template_directory_uri();?>/img/top_prof_img_02.jpg" alt="スクラムキッズアカデミー 代表 にれい あやこ" class="topProfImg">
                </div>
                <div class="text-right mb50">
                    <p class="bold mb0">スクラムキッズアカデミー 代表<!--<br>リトムビューティーアドバイザー--></p>
                    <p class="mainColor h1 bold">にれい あやこ</p>
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
                <div class="mb50">
<p>実家が幼児体操教室という環境で育つ。</p>
<p>学生時代は硬式テニスで数々の全国大会に出場、全国ベスト８の成績を残すが、テニスを心から楽しむ事が出来ず、苦しい学生時代を過ごす。</p>
<p>大学卒業後 大手企業に事務職として就職するが、ストレスにより体調不良に悩まされる。</p>
<p>26歳で出会ったサルサダンスに打ち込んでいく中で、心身ともに健康になりインストラクターに。国内外の舞台で踊り、横浜で行われたコンペティションではプロ部門で優勝を果たす。</p>
<p>子どもの頃から体を動かすことが得意で、ダンスという好きな事に出会い、自己表現の喜びと、心から楽しむことの大切さを実感。</p>
<p>実家の幼児体育研究所で幼児体育指導員をしながら、ピラティスやウォーキングなど学びを広げていく。</p>

<p>出産・子育てを機に、我が子だけでなく子ども達に自分の好きな事や得意な事を見つけて、豊かで心から幸せな人生を送って欲しいと願い、2016年「STUDIO SCLM」を開設。</p>
<p>2020年「スクラムキッズアカデミー」へとリニューアル。その他、大人向け<a href="https://www.nirei-ayako.com/" target="_blank" class="linkText mainColor">「にれいあやこウォーキングスクール」 <i class="fa fa-external-link" aria-hidden="true"></i></a>  主宰。</p>
                </div>
                <div class="topPlofCapa bdBox bgWhite mb30">
                    <p class="bold h5 mb10 titleLeftBd">資格</p>
                    <ul class="text_m">
                    
                        <li>・CEOキッズアカデミー初級・中級・上級　認定講師</li>
                        <li>・日本体育協会ジュニアスポーツ指導員</li>
                        <li>・日本能力開発推進協会認定　チャイルドコーチング</li>
                        <li>・日本能力開発推進協会認定　問いかけ型教育スペシャリスト</li>
                        <li>・チャイルドボディセラピスト1級</li>
                        <li>・モデルインストラクター協会認定　ウォーキングスタイリスト</li>
                        <li>・JPTA公認今村式ウォーキングインストラクター1級</li>
                    
                    </ul>
                </div>
            
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6" data-aos="fade-left">
                <div class="topProfImgWrap mb50">
                    <img src="<?php echo get_template_directory_uri();?>/img/top_prof_img_11.jpg" alt="藤原優子" class="topProfImg">
                </div>
                <div class="text-right mb50">
                    <p class="bold mb0">運営代表</p>
                    <p class="mainColor h1 bold">藤原優子</p>
                </div>
            </div>
            <div class="col-sm-6" data-aos="fade-right">
                <div class="mb50">
                
                    <p>男の子3人(H7,Ｈ9,Ｈ12)、年の差兄弟ママ。</p>
                    <p>長男を育てている時に、育てにくさを感じる。</p>
                    <p>疑問を抱えながら、答えを見つける子育てをしてきた。</p>
                    <p>三男を出産して、出会ったメソッドで、長男の疑問の答えを見つける。</p>
                    <p>「100人いれば100通りの特性を持つ子供達」</p>
                    <p>子どもの「個」を尊重し信頼する子育て、子どもの魅力を最大限引き出し、発揮するための親のあり方・環境づくりなど、いつでも相談できる子どもの専門家として、日々探求し続けている。</p>
                
                </div>
                <div class="topPlofCapa bdBox bgWhite mb30">
                    <p class="bold h5 mb10 titleLeftBd">資格</p>
                    <ul class="text_m">
                    
                        <li>一般社団法人発育発達アソシエイト認定</li>
                        <li>発育発達トレーナー®︎</li>
                        <li>学習発達ティーチャー®︎</li>
                        <li>キャリアマナーコーチ®︎</li>
                        <li>米国NLP™️協会認定マスタープラクティショナー</li>
                        <li>色彩心理診断士</li>
                    </ul>
                </div>
            
            </div>
        </div>
    </div>
</section>



<section class="padding bgGrayColor">
    <div class="container">
        <div class="text-center mb50">
            <div class="mb30">
                <h3 class="bold h1">入会の流れ</h3>
                <p class="fontEn mainColor h4">Flow</p>
            </div>
        </div>
        <div class="width980" data-aos="fade-up">
            <div class="topFlowBox bdBox bgWhite">
                <h5 class="h3 bold mb30"><span class="topFlowBoxNum fontEn bgMainColor white h4">1</span>お申込み</h5>
                <div class="text_m gray">
                    <p>ホームページのお問い合わせフォームか、電話、もしくはLINEでお問い合わせください。</p>
                </div>
            </div>
            <div class="topFlowBox bdBox bgWhite">
                <h5 class="h3 bold mb30"><span class="topFlowBoxNum fontEn bgMainColor white h4">2</span>無料体験（2回まで）</h5>
                <!--<h5 class="h3 bold mb30"><span class="topFlowBoxNum fontEn bgMainColor white h4">2</span>無料体験<span class="h5 bold">【6月末までなら何度でも体験可能】</span></h5>-->
                <div class="text_m gray">
                    <p>実際にスタジオスクラムに来ていただいて、無料体験を受けることができます。</p>
                    <p>現在募集中のクラスの中からお選びすることができます。</p>
                </div>
            </div>
            <div class="topFlowBox bdBox bgWhite">
                <h5 class="h3 bold mb30"><span class="topFlowBoxNum fontEn bgMainColor white h4">3</span>ご希望のクラスを選択</h5>
                <div class="text_m gray">
                    <p>※各クラス定員6〜8名。人数の都合でご希望に沿えない場合はご了承ください。</p>
                </div>
            </div>
            <div class="topFlowBox bdBox bgWhite">
                <h5 class="h3 bold mb30"><span class="topFlowBoxNum fontEn bgMainColor white h4">4</span>ご入会</h5>
                <div class="text_m gray">
                    <p>ご入会頂いた後は翌日から受講が可能です。初月に関しては日割り計算とさせていただきます。</p>
                    <p>月謝は引き落としとなりますので、口座がわかるものと印鑑をご持参ください。</p>
                    <p>※入会手数料5,000円(税別)が入会時に発生します。<br>※年間設備2,000円(税別)、年間スポーツ保険料800円(税別)が発生します。</p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="padding bgBlueLight" id="faq">
    <div class="container">
        <div class="text-center mb50">
            <div class="mb30">
                <h3 class="bold h1">よくあるご質問</h3>
                <p class="fontEn mainColor h4">Q&amp;A</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="topQaBox bdBox bgWhite matchHeight mb30">
                    <h5 class="bold h4 titleBd mb20"><span class="fontEn mainColor h3">Q.</span>休んだ場合、振り替えはできますか？</h5>
                    <p class="text_m gray">開催日であれば、振り替えは可能ですが、人数の都合で都合でお受けできない場合はご了承ください。またオプションコースから基本コース・幼児コースへの振り替えはできません。</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="topQaBox bdBox bgWhite matchHeight mb30">
                    <h5 class="bold h4 titleBd mb20"><span class="fontEn mainColor h3">Q.</span>支払い方法について教えて下さい。</h5>
                    <p class="text_m gray">口座引き落としとなります。翌月分を前月の27日に引き落としとなります。初回は入会金等をまとめて引き落としとなります。</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="topQaBox bdBox bgWhite matchHeight mb30">
                    <h5 class="bold h4 titleBd mb20"><span class="fontEn mainColor h3">Q.</span>下の子を連れて行っていいですか？</h5>
                    <p class="text_m gray">スタジオ一階に待合サロンがございますので、待たれる場合はそちらをご利用ください。絵本やおもちゃもご用意しております。</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="topQaBox bdBox bgWhite matchHeight mb30">
                    <h5 class="bold h4 titleBd mb20"><span class="fontEn mainColor h3">Q.</span>保護者の見学は可能ですか？</h5>
                    <p class="text_m gray">体験レッスンはもちろん見学可能ですが、実際のレッスンの際は基本的にはご退室いただいております。見学の方の小声のお喋りが、お子様の集中力やレッスン遂行の妨げになる場合がある為、ご理解ください。外出いただくか、一階待合サロンをご利用ください。</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="topQaBox bdBox bgWhite matchHeight mb30">
                    <h5 class="bold h4 titleBd mb20"><span class="fontEn mainColor h3">Q.</span>お休みする場合は？</h5>
                    <p class="text_m gray">ライン＠またはお電話にて事前にお知らせください。ご連絡がなかった場合は、振替ができません。振替は１ヶ月以内で受付可能ですが、定員オーバーのクラスではお引き受けできない場合もございます。</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="topQaBox bdBox bgWhite matchHeight mb30">
                    <h5 class="bold h4 titleBd mb20"><span class="fontEn mainColor h3">Q.</span>発表会などはありますか？</h5>
                    <p class="text_m gray">現在のところは、特別発表会の機会は設けておりません。子ども達との話し合いの中で、そのような声が上がってきましたら、ささやかな発表会を行う可能性もございます。</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="topQaBox bdBox bgWhite matchHeight mb30">
                    <h5 class="bold h4 titleBd mb20"><span class="fontEn mainColor h3">Q.</span>駐車場はありますか？</h5>
                    <p class="text_m gray">はい、敷地内駐車場が5台あります。また提携駐車場もございますので、ご利用ください。<br><a href="<?php echo get_template_directory_uri();?>/img/footer_map_car.jpg" class="linkText mainColor" data-lightbox="lightbox">詳しい地図はこちらから <i class="fa fa-external-link" aria-hidden="true"></i></a></p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="topQaBox bdBox bgWhite matchHeight mb30">
                    <h5 class="bold h4 titleBd mb20"><span class="fontEn mainColor h3">Q.</span>子どもがやりたがらない場合はどうしたらいいですか？</h5>
                    <p class="text_m gray">お子様にも気がのらない理由があるはずです。その気持ちに寄り添い、解決方法をお子様と親御さんとともに、話合いをさせていただきたいと思います。</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="topQaBox bdBox bgWhite matchHeight mb30">
                    <h5 class="bold h4 titleBd mb20"><span class="fontEn mainColor h3">Q.</span>休会や退会について</h5>
                    <p class="text_m gray">「休会届」「退会届」のご提出が必要です。提出期限は前月の10日までとなります。10日を過ぎますと、翌月の引落がかかってしまいますので、休会・退会をお希望の方は、必ず10日までにご提出ください。休会から復帰後、定員オーバーの場合はキャンセル待ちになる場合もございますのでご了承ください。</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="text-center mb50">
            <div class="mb30">
                <h3 class="bold h1">新着情報</h3>
                <p class="fontEn mainColor h4">News</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="topNewsBox bgWhite bdBox mb30">
                    <h5 class="bold h3 titleBd mb50 text-center">お知らせ</h5>
                    <ul class="topNewsBoxUl mb50 matchHeight">
                    
                        <?php
                            //$paged = (get_query_var('page')) ? get_query_var('page') : 1;
                            $paged = get_query_var('page');
                            $args = array(
                                'post_type' =>  'post', // 投稿タイプを指定
                                'paged' => $paged,
                                'posts_per_page' => 6, // 表示するページ数
                                'cat' => -4,
                                'orderby'=>'date',
                                'order'=>'DESC'
                                        );
                            $wp_query = new WP_Query( $args ); // クエリの指定 	
                            while ( $wp_query->have_posts() ) : $wp_query->the_post();
                                //ここに表示するタイトルやコンテンツなどを指定 
                            get_template_part('content-post-top'); 
                            endwhile;
                            //wp_reset_postdata(); //忘れずにリセットする必要がある
                            wp_reset_query();
                        ?>		
                    
                        <li>
                    </ul>
                    <div class="text-center">
                        <a href="<?php echo home_url();?>/news" class="h5 white button bold tra text-center">詳しく見る</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="topNewsBox bgWhite bdBox mb30">
                    <h5 class="bold h3 titleBd mb50 text-center">お役立ち情報</h5>
                    <ul class="topNewsTopicBoxUl mb50 matchHeight">
                    
                        <?php
                            //$paged = (get_query_var('page')) ? get_query_var('page') : 1;
                            $paged = get_query_var('page');
                            $args = array(
                                'post_type' =>  'post', // 投稿タイプを指定
                                'paged' => $paged,
                                'posts_per_page' => 3, // 表示するページ数
                                'cat' => 4,
                                'orderby'=>'date',
                                'order'=>'DESC'
                                        );
                            $wp_query = new WP_Query( $args ); // クエリの指定 	
                            while ( $wp_query->have_posts() ) : $wp_query->the_post();
                                //ここに表示するタイトルやコンテンツなどを指定 
                            get_template_part('content-post-topic'); 
                            endwhile;
                            //wp_reset_postdata(); //忘れずにリセットする必要がある
                            wp_reset_query();
                        ?>		
                    </ul>
                    <div class="text-center">
                        <a href="<?php echo home_url();?>/category/topic/" class="h5 white button bold tra text-center">詳しく見る</a>
                    </div>
                </div>
            </div>
        </div>
        <a href="https://www.instagram.com/studio_sclm_kids/" target="_blank">
            <img src="<?php echo get_template_directory_uri();?>/img/banner_insta.png" alt="インスタグラム" class="bannerInsta opa tra">
        </a>
    </div>
</section>





</main>



<?php get_footer(); ?>