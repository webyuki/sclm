<?php get_header(); ?>
<main>

<section class="bgStripe pageHeader">
    <div class="container">
        <div class="text-center">
            <h3 class="bold h1">お知らせ</h3>
            <p class="fontEn mainColor h4">News</p>
        </div>
    </div>
</section>




        

<section class="pageNews margin">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php
					while ( have_posts() ) : the_post();
						get_template_part('content-post'); 
					endwhile;
				?>
			</div>
			<div class="col-sm-3">
				<?php dynamic_sidebar(); ?>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>
</main>
<?php get_footer(); ?>