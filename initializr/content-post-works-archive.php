            <div class="col-sm-4">
                <a href="<?php the_permalink(); ?>" class="">
                    <div class="topWorksBox mb30">
                    
						<?php if (has_post_thumbnail()):?>
							<?php 
								// アイキャッチ画像のIDを取得
								$thumbnail_id = get_post_thumbnail_id();
								// mediumサイズの画像内容を取得（引数にmediumをセット）
								$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
								$eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
							?>
                                <div class="topWorksImg bgImg mb10 relative tra" style="background-image:url('<?php echo $eye_img_s[0];?>')">
							<?php else: ?>
                                <div class="topWorksImg bgImg mb10 relative tra" style="background-image:url('<?php echo get_template_directory_uri();?>/img/sample01.png')">
						<?php endif; ?>

                            <div class="topWorksCate absolute bgMainColor white text_m bold">

                                <?php 
                                    if ($terms = get_the_terms($post->ID, 'works_cat')) {
                                        foreach ( $terms as $term ) {
                                            echo '<span>' . esc_html($term->name) .'</span>';
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                        <h5 class="bold h4"><?php the_title(); ?></h5>
                        <p class="gray text_m">
                        
                            <?php 
                                if ($terms = get_the_terms($post->ID, 'works_area')) {
                                    foreach ( $terms as $term ) {
                                        echo '<span>' . esc_html($term->name) .'</span>';
                                    }
                                }
                            ?>
                        
                        </p>
                    </div>
                </a>
            </div>