<?php get_header(); ?>

<main>

<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_about_fv.jpg">
        <div class="bgWhiteTrans paddingW">
            <div class="container" data-aos="fade-up">
                <div class="text-center">
                    <p class="fontEn h3 mb0 mainColor">ABOUT US</p>
                    <h3 class="h2 bold">オカザキリフォームラボについて</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">MESSAGE</p>
            <h3 class="h3 bold">代表あいさつ</h3>
        </div>
        <div class="row">
            <div class="col-sm-6 mb30" data-aos="fade-right">
                <h4 class="topServiceTitle  h3 mb30"><span class="bold">地元玉野に住むお客さまの、<br>快適な暮らしのために</span></h4>
                <div class="mb30">

<p>一般住宅にお風呂が普及したのは、太平洋戦争が終わった後だと言われています。</p>
<p>当時すでに住宅建築に関わっていた岡崎建材（現・オカザキリフォームラボ）は、個人の住宅にお風呂を作る工事を中心として、水まわりを得意とする施工店へとサービスを広げてまいりました。</p>


                </div>
                <h5 class="mb10 bold h4">風呂工事から暮らしのパートナーへ</h5>
                <div class="mb30">
                
<p>毎日家族をあたためたお風呂は、ときに水漏れやタイルのひび割れを起こします。私たちは修理や点検を通じて、お客さまとのつながりを深めてきました。</p>

<p>若いご夫婦が家を建て、お子さんを迎え、巣立ちを見届け、そして高齢となるまで。住まいは幾度も、ご家族のライフスタイルにあわせてその形を変えてきました。</p>

<p>この地域に住むお客さまがいつも安心して暮らせるように、もっと私たちにできることはないだろうか。ひたむきに育てたサービスは今、「家一軒、丸ごとお願いしたい」とのリクエストにもお応えできるまでに充実しました。</p>
                
                
                </div>
                <h5 class="mb10 bold h4">安心と信頼を守る技術をこれからも</h5>
                <div class="mb30">


<p>住宅に使用する設備は、各メーカーが様々な商品を開発しており、工事にもいろいろなやり方があります。</p>

<p>単純に費用をおさえることは、決して難しいことではありません。</p>

<p>けれど私たちは目先の見た目だけでなく、お客さまが選んだ設備が一日でも長く、少しでも生活にかかる負担を減らして活躍できるように、トップクラスの技術をもって施工を進めてまいります。</p>

<p>明日からもずっと、ここで安心して暮らしていくために。</p>

<p>私たちはこれからも、お客さまの住まいのパートナーであり続けます。</p>

                </div>
                <p class="text_m mb0">代表取締役</p>
                <p class="h3 bold">岡崎 晋典</p>
                
            </div>
            <div class="col-sm-6" data-aos="fade-left">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_about_message_01.jpg" alt="">
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">FEATURE</p>
            <h3 class="h3 bold">オカザキリフォームラボの特徴</h3>
        </div>
        <div class="row pageAboutFeatureRow" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_about_feature_01.jpg" alt="">
                    <p class="pageAboutFeatureNum absolute fontEn h00 mainColor">01</p>
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">地域密着型ならではの提案</span></h4>
                <div class="mb30">

<p>オカザキリフォームラボは、”岡崎建材”として90年、地元玉野で営業を続け、新築だったお住いが年月を経て、次の世代へと受け継がれていく様子を見守ってきました。</p>
<p>住まいと住む人が時間を重ね、時代と家族がめまぐるしく変化した90年。町とご家族の歴史を知る私たちだからこそ、お客さまに寄り添い、ご家族の未来のためにできることがあります。</p>

                </div>
            </div>
        </div>
        <div class="row pageAboutFeatureRow" data-aos="fade-up">
            <div class="col-sm-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_about_feature_02.jpg" alt="">
                    <p class="pageAboutFeatureNum absolute fontEn h00 mainColor left">02</p>
                </div>
            </div>
            <div class="col-sm-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">職種を超えたネットワーク</span></h4>
                <div class="mb30">

<p>建材屋としての歴史を長く持つオカザキリフォームラボでは、大工や左官といった職人をはじめ、設計士やデザイナー、ときには税や法律を扱うプロフェッショナルともつながりを深めてきました。</p>
<p>自社のスキルアップだけでなく、人的ネットワークを構築し、「住まいに悩むお客さまの、あらゆるご相談に応えるリフォーム店」として活躍の場を広げています。</p>

                </div>
            </div>
        </div>
        <div class="row" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_about_feature_03.jpg" alt="">
                    <p class="pageAboutFeatureNum absolute fontEn h00 mainColor">03</p>
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">長く効率よく使うことを<br>前提とした施工</span></h4>
                <div class="mb30">

<p>住宅や、住宅に使用される設備はお客さまの財産。私たちがそれにふれるのは、その価値を上げるためであるという自覚を持って工事に臨みます。</p>
<p>ひとつ一つの設備が長く、お客さまの幸せな暮らしによい影響をもたらすものであるために。持てる限りの知識と技術を駆使し、最善のご提案と施工を進めてまいります。</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="row" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="mb30">
                    <p class="fontEn h4 mb0 mainColor">COMPANY</p>
                    <h3 class="h3 bold">会社概要</h3>
                </div>

                <div class="pageAboutCompanyUl mb50">
                    <ul>
                        <li>会社名</li>
                        <li>株式会社岡崎建材 / オカザキリフォームラボ</li>
                    </ul>
                    <ul>
                        <li>代表者名</li>
                        <li>岡崎 晋典</li>
                    </ul>
                    <ul>
                        <li>創業</li>
                        <li>1930年3月</li>
                    </ul>
                    <ul>
                        <li>資本金</li>
                        <li>1000万円</li>
                    </ul>
                    <ul>
                        <li>住所</li>
                        <li>岡山県玉野市玉6丁目18-7</li>
                    </ul>
                    <ul>
                        <li>連絡先</li>
                        <li>TEL:050-3196-9159<br>FAX:0863-31-1270</li>
                    </ul>
                    <ul>
                        <li>営業時間</li>
                        <li>AM8：00～PM6：00<br> （日曜、祝日、年末年始は除く）</li>
                    </ul>
                    <ul>
                        <li>加盟団体</li>
                        <li>ＴＯＴＯリモデルクラブコンタクトメンバーズ<br>クリナップ水まわり工房<br>ＴＯＴＯ水彩工房</li>
                    </ul>
                    <ul>
                        <li>取得資格</li>
                        <li>

    2級建築士<br>
    建築施工管理技士<br>
    給水装置主任技術者<br>
    福祉環境コーディネーター2級<br>
    2級管工事施工管理技士<br>
    増改築相談員					
                        </li>
                    </ul>
                    <ul>
                        <li>事業内容</li>
                        <li>

    ・住まいに関わる全ての工事や修繕<br>
    ・住まいに関わる全ての建材や設備機器の販売<br>
    ・土木工事に関する製品の販売

                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_about_company_01.jpg" alt="">
                <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_about_company_02.jpg" alt="">
                <div class="map">
                    <iframe class="company_map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13154.824343850933!2d133.9186136242676!3d34.48497961857912!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x625385f4e3d442e9!2z77yI5qCq77yJ5bKh5bSO5bu65p2Q44O744Kq44Kr44K244Kt44Oq44OV44Kp44O844Og44Op44Oc!5e0!3m2!1sja!2sjp!4v1572499669531!5m2!1sja!2sjp" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>