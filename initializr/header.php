<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!--キャッシュクリア-->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!--キャッシュクリア終了-->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/x-icon" href="<?php echo home_url();?>/favicon.ico">
<title><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri();?>/apple-touch-icon.png">
<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/modal.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/loaders.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/lightbox/dist/css/lightbox.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap" rel="stylesheet">

<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/slick/slick.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/slick/slick-theme.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/main.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">

<script src="<?php echo get_template_directory_uri();?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri();?>/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="<?php echo get_template_directory_uri();?>/js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/slick/slick.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/lightbox/dist/js/lightbox.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.matchHeight-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/parallax/parallax.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/main.js"></script>
<script>
/*高さ合わせる*/
$(function(){
	$('.matchHeight').matchHeight();
});

</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NSW6BC9');</script>
<!-- End Google Tag Manager -->

</head>

<body <?php body_class();?>>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NSW6BC9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<!--ローディングCSS-->
<!--
<div id="js-loader" class="bgMainColor">
	<div class="loader-inner ball-pulse">
	  <div></div>
	  <div></div>
	  <div></div>
	</div>
</div>
-->
<style>
#js-loader {
	display: flex;
    position: fixed;
    height: 100vh;
    width: 100vw;
    z-index: 9999;
    align-items: center;
    justify-content: center;
}
</style>

<script>
// ローディング画面をフェードインさせてページ遷移
$(function(){
    // リンクをクリックしたときの処理。外部リンクやページ内移動のスクロールリンクなどではフェードアウトさせたくないので少し条件を加えてる。
    $('a[href ^= "https://newstella.co.jp"]' + 'a[target != "_blank"]').click(function(){
        var url = $(this).attr('href'); // クリックされたリンクのURLを取得
        $('#js-loader').fadeIn(600);    // ローディング画面をフェードイン
        setTimeout(function(){ location.href = url; }, 800); // URLにリンクする
        return false;
    });
});
 
// ページのロードが終わった後の処理
$(window).load(function(){
  $('#js-loader').delay(300).fadeOut(400); //ローディング画面をフェードアウトさせることでメインコンテンツを表示
});
 
// ページのロードが終わらなくても10秒たったら強制的に処理を実行
$(function(){ setTimeout('stopload()', 10000); });
function stopload(){
  $('#js-loader').delay(300).fadeOut(400); //ローディング画面をフェードアウトさせることでメインコンテンツを表示
}

</script>






<header>
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<div class="headerLeft">
					<div class="headerText"><h2 class="text_s gray">4才〜10才までの人材育成スタジオ</h2></div>
					<a href="<?php echo home_url();?>"><h1 class="logo remove"><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></h1></a>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="headerRight pc">
					<div class="headerRightContact">
                        <a href="<?php echo home_url();?>/contact" class="h5 white button bgGreen bold tra"><i class="fa fa-envelope-o h4" aria-hidden="true"></i><span class="bold">お問い合わせ</span></a>
					</div>
					<div class="headerRightTel text-right">
                        <a href="<?php echo home_url();?>/#calendar" class="h5 white button bgYellow bold tra"><i class="fa fa-smile-o h4" aria-hidden="true"></i><span class="bold">無料体験に申し込む</span></a>
					</div>
					<div class="headerRightTel text-right hidden-sm">
                        <a href="line://ti/p/%40pmn7040l" target="_blank" class="h5 white button bold tra line"><i class="fa fa-comment-o h4" aria-hidden="true"></i><span class="bold">LINEで相談する</span></a>
					</div>
				</div>
			</div>
		</div>
        <div class="headerMenu pc text-center">
            <?php wp_nav_menu( array( 'menu_class' => 'flex justCenter alignCenter bold' ) ); ?>
        </div>
	</div>
</header>


<!-- 開閉用ボタン -->

<div class="menu-btn sp" id="js__btn">
    <!--<span data-txt-menu="MENU" data-txt-close="CLOSE"></span>-->
	<div class="menu-trigger" href="#">
		<span></span>
		<span></span>
		<span></span>
	</div>	
</div>

<!-- モーダルメニュー -->
<nav class="overRayMenu menu bgMainColor" id="js__nav">
    <a class="logoLink" href="<?php echo home_url();?>"><p class="logo remove"></p></a>

	<?php wp_nav_menu( array( 'menu_class' => 'fontEn overRayMenuUl white' ) ); ?>
</nav>



<section class="contactSide absolute pc">
	<a href="<?php echo home_url();?>/#calendar" class="contactSideWap text-center">
		<div class="fontEn mainColor h5 titleBdContact">Lesson</div>
		<div class="titleJp mColor text_s">無料体験<span class="">は<br>こちらから</span></div>
	</a>
	
</section>


<!--
<section class="footerTelSp sp bgMainColor">
	<ul class="LiInlineBlock">
		<li class="footerTelSpLi1 white text-center">
			<div class="text_ss">ご予約・お問い合わせはこちら</div>
			<div class="footerTelSpLi1Tel h3 bold">
				<a href="tel:05031969159"><i class="fa fa-phone" aria-hidden="true"></i><span>050-3196-9159</span></a>
			</div>
		</li >
		<li class="footerTelSpLi2 text_m mainColor"><a href="<?php echo home_url();?>/contact">お問い合わせ</a></li>
	</ul>
	
</section>

-->


<section class="footerTelSp sp">
	<ul class="flex">
		<li class="footerTelSpFlexLi text-center white">
			<a href="tel:05031969159"><i class="fa fa-phone" aria-hidden="true"></i><div class="text_s bold">お電話</div></a>
		</li>
		<li class="footerTelSpFlexLi text-center white">
			<a href="<?php echo home_url();?>/#calendar" target=""><i class="fa fa-user-o" aria-hidden="true"></i><div class="text_s bold">無料体験</div></a>
		</li>
		<li class="footerTelSpFlexLi text-center white">
			<a href="<?php echo home_url();?>/contact"><i class="fa fa-envelope-o" aria-hidden="true"></i><div class="text_s bold">お問合せ</div></a>
		</li>
		<li class="footerTelSpFlexLi text-center white">
			<a href="line://ti/p/%40pmn7040l" target="_blank"><i class="fa fa-comment-o" aria-hidden="true"></i><div class="text_s bold">LINE</div></a>
		</li>
	</ul>
    <!--
	<ul class="flex">
		<li class="footerTelSpFlexLi text-center mainColor bgSubColor">
			<a href="tel:0862507810"><i class="fa fa-phone" aria-hidden="true"></i><div class="text_s bold">居宅介護</div></a>
		</li>
		<li class="footerTelSpFlexLi text-center mainColor bgMainColor">
			<a href="tel:0862505255"><i class="fa fa-phone" aria-hidden="true"></i><div class="text_s bold">訪問介護</div></a>
		</li>
		<li class="footerTelSpFlexLi text-center white bgGrayColor">
			<a href="<?php echo home_url();?>/contact"><i class="fa fa-user-o" aria-hidden="true"></i><div class="text_s bold">お問い合わせ</div></a>
		</li>
	</ul>
    -->
</section>



<script>

$(function () {
    $()
});
$(function () {
    var $body = $('body');

    //開閉用ボタンをクリックでクラスの切替え
    $('#js__btn').on('click', function () {
        $body.toggleClass('open');
        $('.menu-trigger').toggleClass('active');
        
    });

    //メニュー名以外の部分をクリックで閉じる
    $('#js__nav').on('click', function () {
        $body.removeClass('open');
        $('.menu-trigger').removeClass('active');
    });
});

</script>

