<?php get_header(); ?>

<main>


<section class="bgStripe pageHeader">
    <div class="container">
        <div class="text-center">
            <h3 class="bold h1">確認画面</h3>
            <p class="fontEn mainColor h4">Confirm</p>
        </div>
    </div>
</section>


<section class="margin">
	<div class="container">
		<div class="">
			<div class="contInCont" data-aos="fade-up">
				<div class="mb30 text-center width780">
					<p>こちらの内容でお間違いないでしょうか？</p>
					<p>問題なければ下記の送信ボタンを押して下さい。</p>
				</div>
				<a class="telLink fontNum h0 text-center bold mainColor block mb0" href="tel:05031969159">050-3196-9159</a>
                <p class="gray mb30 text-center">AM9：30～PM6：00 （日曜、祝日除く）</p>
				<div class="contactForm" data-aos="fade-up"><?php echo do_shortcode('[mwform_formkey key="39"]'); ?></div>
			</div>
		</div>
	</div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>