
<?php get_template_part( 'parts/temp-contact' ); ?>				




<section class="sectionPankuzu">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
	</div>
</section>

<footer class="" id="map">
    <div class="container">
        <div class="row mb50">
            <div class="col-sm-5">
                <div class="text_m">
                    <p class="">4才〜10才までの人材育成スタジオ</p>
                    <a href="<?php echo home_url();?>"><p class="logo logo_footer remove mb30 ml0"><?php the_title();?></p></a>
                    
                    <ul class="footerAccessTextUl inlineBlockUl">
                        <li>店舗名</li>
                        <li>STUDIO SCLM スタジオスクラム<br>
                            <a href="https://www.nirei-ayako.com/" target="_blank" class="linkText mainColor">大人向けウォーキングスクールはこちら <i class="fa fa-external-link" aria-hidden="true"></i></a>
</li>
                    </ul>
                    <ul class="footerAccessTextUl inlineBlockUl">
                        <li>住所</li>
                        <li>〒700‒0925 岡山市北区大元上町7‒20<a target="_blank" href="https://goo.gl/maps/d6xfumNzP3Mo8aem7"><i class="fa fa-map-marker"></i></a></li>
                    </ul>
                    <ul class="footerAccessTextUl inlineBlockUl">
                        <li>Tel</li>
                        <li>050-3196-9159</li>
                    </ul>
                    <ul class="footerAccessTextUl inlineBlockUl">
                        <li>Mail</li>
                        <li>info@studio‒sclm.com</li>
                    </ul>
                    <ul class="footerAccessTextUl inlineBlockUl">
                        <li>駐車場</li>
                        <li>5台（その他提携の駐車場がございます）<br>
                        <a href="<?php echo get_template_directory_uri();?>/img/footer_map_car.jpg" class="linkText mainColor" data-lightbox="lightbox">詳しい地図はこちらから <i class="fa fa-external-link" aria-hidden="true"></i></a></li>
                    </ul>
                    <ul class="footerAccessTextUl inlineBlockUl">
                        <li>アクセス</li>
                        <li>《バス》備前野田バス停 徒歩7分／大元上町バス停 徒歩6分<br>《電車》大元駅／北長瀬駅<br>《 車 》岡山駅から10分。問屋町から３分。</li>
                    </ul>

                </div>
            </div>
            <div class="col-sm-3">
                <div class="googlemap">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10619.726976327744!2d133.8906333877086!3d34.651895537110114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x355407738f49555f%3A0x4d2f9b5e632cdd3f!2z44K544K_44K444Kq44K544Kv44Op44Og!5e0!3m2!1sja!2sjp!4v1577238387453!5m2!1sja!2sjp" width="100%" height="230" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
            <div class="col-sm-4">
                <img src="<?php echo get_template_directory_uri();?>/img/footer_map.png" alt="" class="">
            </div>
        </div>
        <div class="wrapper_copy">
            <div class="container">
                <p class="text-center text_s gray">copyright© 2019 studio sclm all rights reserved.</p>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>



<script>
AOS.init({
	placement	: "bottom-top",
	duration: 1000,
});
</script>
</body>
</html>
 