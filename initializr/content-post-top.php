<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>


                        <li>
                            <a href="<?php the_permalink();?>">
                                <p class="bold mb0"><?php the_title();?></p>
                                <p class="mainColor text_m fontEn titleLeftBd bdPinkLeft"><?php the_time('y/m/d'); ?></p>
                            </a>
                        </li>