<?php get_header(); ?>
<main>

<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>

<section class="bgStripe pageHeader">
    <div class="container">
        <div class="text-center">
            <h3 class="bold h1"><?php echo get_the_archive_title(); ?></h3>
            <p class="fontEn mainColor h4">News</p>
        </div>
    </div>
</section>







<section class="pageNews margin">
	<div class="container">
		<?php //get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<?php
					while ( have_posts() ) : the_post();
						get_template_part('content-post'); 
					endwhile;
				?>
			</div>
			<div class="col-sm-3">
				<?php dynamic_sidebar(); ?>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>

</main>


<?php get_footer(); ?>