<section class="padding bgStripe">
    <div class="container">
        <div class="text-center mb50">
            <div class="mb30">
                <h3 class="bold h1">まずは<span class="bold mainColor">無料体験</span>してみてください</h3>
                <p class="fontEn mainColor h4">Try it!</p>
            </div>
        </div>
        <div class="bdBox bgWhite topContactBox" data-aos="fade-up">
            <div class="row">
                <div class="col-sm-7">
                    <!--<p class="bold h5 mb10 text-center lineThrough">【2回まで無料で可能】</p>-->
                    <p class="bold h3 mb10 text-center">【2回まで無料で可能】</p>
                    <!--<p class="bold h3 titleBd mb30 text-center">【6月なら何度でも体験可能！】</p>-->
                    <p class="lh_xl bold text-center">スクラムの魅力は体験してみないとわかりません。<br>だからスクラムは入会の前に、無料体験をお勧めしています。<br>ご両親・お子様がどちらも納得して入会してほしいから、<br>2回まで無料体験可能としています。</p>
                </div>
                <div class="col-sm-5">
                    <div class="text-center">
                        <a href="<?php echo home_url();?>/contact" class="h5 white button bgGreen bold tra mb10"><i class="fa fa-envelope-o h4" aria-hidden="true"></i><span class="bold">お問い合わせ</span></a>
                        <a href="<?php echo home_url();?>/#calendar" class="h5 white button bgYellow bold tra mb10"><i class="fa fa-smile-o h4" aria-hidden="true"></i><span class="bold">無料体験<span class="hidden-sm bold">に申し込む</span></span></a>
                        <a href="line://ti/p/%40pmn7040l" target="_blank" class="h5 white button line bold tra mb20"><i class="fa fa-comment-o h4" aria-hidden="true"></i><span class="bold">LINEで相談する</span></a>
                        
                        <a href="tel:05031969159"><p class="fontEn h2 mb0"><i class="fa fa-phone mainColor" aria-hidden="true"></i>050-3196-9159</p></a>
                        <p class="text_m gray">10:00～18:00</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>