		<div class="row">
			<div class="col-sm-6" data-aos="fade-up">
				<div class="topServiceBox" style="background-image:url('<?php echo get_template_directory_uri();?>/img/ico_shiroari.png')">
					<div class="bgWhite topServiceBoxWhite">
						<img class="topServiceBoxIco mb30" src="<?php echo get_template_directory_uri();?>/img/ico_shiroari.png" alt="">
						<h4 class="bold text-center h3 mb30">シロアリ<br>駆除・予防</h4>
						<p class="text_m grayColor mb30">シロアリは木造家屋以外の建物にも被害を与えます。地中から床下に侵入するため被害の大きさがわかりづらく、完全な駆除が難しい害虫です。少しでも気になる場合はプロにご相談ください。</p>
						<ul class="inline_block bold mainColor mb30">
							<li class="topServiceBoxLiFee">料金</li>
							<li class="topServiceBoxLiText">㎡／<span class="h3 bold">1,200</span>円（税別）～</li>
						</ul>
						<a href="<?php echo home_url();?>/service/shiroari" class="button white tra text-center">詳しく見る</a>
					</div>
				</div>
			</div>
			<div class="col-sm-6 topServiceBox2nd" data-aos="fade-up">
				<div class="topServiceBox" style="background-image:url('<?php echo get_template_directory_uri();?>/img/ico_hachi.png')">
					<div class="bgWhite topServiceBoxWhite">
						<img class="topServiceBoxIco mb30" src="<?php echo get_template_directory_uri();?>/img/ico_hachi.png" alt="">
						<h4 class="bold text-center h3 mb30">ハチ<br>駆除</h4>
						<p class="text_m grayColor mb30">ハチは建物の新旧にかかわらず、非常に小さな隙間を見つけて侵入します。床下・天井裏などの狭い場所や駆除効果の高い夜間の作業、巣の後処理なども安心してお任せください。</p>
						<ul class="inline_block bold mainColor mb30">
							<li class="topServiceBoxLiFee">料金</li>
							<li class="topServiceBoxLiText"><span class="h3 bold">5,000</span>円～（税別）</li>
						</ul>
						<a href="<?php echo home_url();?>/service/hachi" class="button white tra text-center">詳しく見る</a>
					</div>
				</div>
			</div>
			<div class="col-sm-6 topServiceBox3rd" data-aos="fade-up">
				<div class="topServiceBox" style="background-image:url('<?php echo get_template_directory_uri();?>/img/ico_goki.png')">
					<div class="bgWhite topServiceBoxWhite">
						<img class="topServiceBoxIco mb30" src="<?php echo get_template_directory_uri();?>/img/ico_goki.png" alt="">
						<h4 class="bold text-center h3 mb30">ゴキブリ / ネズミ 駆除・予防<br>（飲食店様向け）</h4>
						<p class="text_m grayColor mb30">衛生面での注意が必要な店舗様を中心に、月に1度の定期点検と駆除・予防を行っています。侵入経路の遮断と安全な薬剤の散布により、年間を通じて清潔な環境を守ります。</p>
						<ul class="inline_block bold mainColor mb30">
							<li class="topServiceBoxLiFee">料金</li>
							<li class="topServiceBoxLiText">月／<span class="h3 bold">5,000～</span>円（税別）</li>
						</ul>
						<a href="<?php echo home_url();?>/service/goki" class="button white tra text-center">詳しく見る</a>
					</div>
				</div>
			</div>
			<div class="col-sm-6 topServiceBox4th" data-aos="fade-up">
				<div class="topServiceBox" style="background-image:url('<?php echo get_template_directory_uri();?>/img/ico_gai.png')">
					<div class="bgWhite topServiceBoxWhite">
						<img class="topServiceBoxIco mb30" src="<?php echo get_template_directory_uri();?>/img/ico_gai.png" alt="">
						<h4 class="bold text-center h3 mb30">害虫 / 害獣<br>駆除</h4>
						<p class="text_m grayColor mb30">ダニ・ノミなどの害虫や、イタチなどの害獣（益獣）による被害は、アレルギーや呼吸器系疾患など、健康にも悪い影響を及ぼします。根本的な駆除は知識と経験豊かなプロにお任せください。</p>
						<ul class="inline_block bold mainColor mb30">
							<li class="topServiceBoxLiFee">料金</li>
							<li class="topServiceBoxLiText"><span class="h3 bold">お問い合わせください</span></li>
						</ul>
						<a href="<?php echo home_url();?>/service/gai" class="button white tra text-center">詳しく見る</a>
					</div>
				</div>
			</div>
		</div>
