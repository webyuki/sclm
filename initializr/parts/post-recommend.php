<?php
//カテゴリ情報から関連記事を10個ランダムに呼び出す
$categories = get_the_category($post->ID);
$category_ID = array();
foreach($categories as $category):
  array_push( $category_ID, $category -> cat_ID);
endforeach ;
$args = array(
  'post__not_in' => array($post -> ID),
  'posts_per_page'=> 10,
  'category__in' => $category_ID,
  'orderby' => 'rand',
);
$query = new WP_Query($args); ?>


<div class="post-recommend">
	<h4 class="text-center">次に読んでほしい記事はこちら</h4>
	<div class="text-center">

  <?php if( $query -> have_posts() ): ?>
  <?php while ($query -> have_posts()) : $query -> the_post(); ?>
  
		<a href="<?php the_permalink(); ?>">
			<p><?php the_title(); //記事のタイトル?></p>
		</a>
  
  
  
  <?php endwhile;?>
  
  <?php else:?>
  <p>記事はありませんでした</p>
  <?php
endif;
wp_reset_postdata();
?>
	</div>
</div>



