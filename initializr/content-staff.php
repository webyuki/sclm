<li>
	<!-- 値の取得 START -->
    <?php
		$main_picture = wp_get_attachment_image_src(get_post_meta($post->ID, 'main_picture', true), 'full');
		$staff_last_name = get_post_meta(get_the_ID(), "last_name", true);
		$staff_last_name_ruby = get_post_meta(get_the_ID(), "last_name_ruby", true);
		$staff_name = get_post_meta(get_the_ID(), "name", true);
		$staff_name_ruby = get_post_meta(get_the_ID(), "name_ruby", true);
		$staff_position = get_post_meta(get_the_ID(), "position", true);
		$staff_license = get_post_meta(get_the_ID(), "license", true);
		$staff_hobby = get_post_meta(get_the_ID(), "hobby", true);
		$staff_message = get_post_meta(get_the_ID(), "message", true);
		$sub_picture = wp_get_attachment_image_src(get_post_meta($post->ID, 'sub_picture', true), 'full');
	?>
    <!-- 値の取得 END -->
	
    <!-- スタッフ紹介 上部分 START -->
    <div class="row staff_introduction_up">
    	<!-- メイン画像エリア START -->
		<div class="col-sm-6">
        	<div><img class="img-responsive" src="<?php echo $main_picture[0]; ?>"></div> <!-- メイン画像の表示 -->
		</div>
        <!-- メイン画像エリア END -->
                    
		<!-- プロフィールエリア START -->
		<div class="col-sm-6">
			<table class="table table-responsive .table_text staff_profile">
				<tr>
					<td class="table_head">名前</td>
					<td class="staff_name"><ruby style="ruby-position: above; margin-bottom: 2px;"><rb><?php echo $staff_last_name; ?><rt><?php echo $staff_last_name_ruby; ?></ruby> <ruby style="ruby-position: above; margin-bottom: 2px;"><rb><?php echo $staff_name; ?><rt><?php echo $staff_name_ruby; ?></ruby><br></td> <!-- 名前の表示 -->
				</tr>
				<tr>
					<td class="table_head">役職</td>
					<td><?php echo $staff_position; ?></td> <!-- 役職の表示 -->
				</tr>
				<tr>
					<td class="table_head">資格</td>
					<td><?php echo $staff_license; ?></td> <!-- 資格の表示 -->
				</tr>
				<tr>
					<td class="table_head">趣味</td>
					<td><?php echo $staff_hobby; ?></td> <!-- 趣味の表示 -->
				</tr>
			</table>
		</div>
		<!-- プロフィールエリア END -->
	</div>
	<!-- スタッフ紹介 上部分 END -->
    
	<!-- スタッフ紹介 下部分 START -->
    <div class="row row_second">
   		<!-- お客様へのメッセージエリア START -->
		<div class="col-sm-6">
			<div>
				<div style="background-color: #D6EBDF;">
					<p class="customer_message">お客様へのメッセージ</p>
				</div>
					<p><?php echo $staff_message; ?></p> <!-- お客様へのメッセージの表示 -->
			</div>
		</div>
        <!-- お客様へのメッセージエリア END -->
		
        <!-- サブ画像エリア START -->
        <div class="col-sm-6">
			<img class="img-responsive" src="<?php echo $sub_picture[0]; ?>"> <!-- サブ画像の表示 -->
		</div>
        <!-- サブ画像エリア END -->
	</div>                    
	<!-- スタッフ紹介 上部分 END -->
</li>







